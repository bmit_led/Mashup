package ch.axa.rest;


import ch.axa.rest.model.PeopleRepositoryMock;
import ch.axa.rest.model.Person;
import org.junit.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class PeopleRepositoryTest {

	private PeopleRepositoryMock peopleRepositoryMock;

	@Before
	public void before() {
		peopleRepositoryMock = new PeopleRepositoryMock();
	}

	@Test
	public void testStart() {
		assertEquals(5, peopleRepositoryMock.getPeople().size());
	}

	@Test
	public void testElement() {
		Optional<Person> person = peopleRepositoryMock.getPerson(peopleRepositoryMock.getPeople().get(0).getId());
		assertTrue(person.isPresent());
		assertEquals(peopleRepositoryMock.getPeople().get(0).getId(), person.get().getId());
		assertTrue(person.get().getId()>0);
	}

	@Test
	public void testAddPerson() {
		Person newPerson = new Person(0, "Peter", "Muster", "pm@musterhausen.ch");
		Person addedPerson = peopleRepositoryMock.addPerson(newPerson);


		Optional<Person> person = peopleRepositoryMock.getPerson(peopleRepositoryMock.getPeople().get(0).getId());
		assertTrue(person.isPresent());
		assertEquals(peopleRepositoryMock.getPeople().get(0).getId(), person.get().getId());
		assertTrue(person.get().getId()>0);
	}

	@Test
	public void testDelete() {
		List<Person> listBefore = peopleRepositoryMock.getPeople();
		int sizeBefore = listBefore.size();
		List<Integer> idsBefore = listBefore.stream().map(person -> person.getId()).collect(Collectors.toList());

		int idDeleted = listBefore.get(0).getId();
		peopleRepositoryMock.deleteById(listBefore.get(0).getId());

		List<Person> listAfter = peopleRepositoryMock.getPeople();
		int sizeAfter = listAfter.size();
		List<Integer> idsAfter = listAfter.stream().map(person -> person.getId()).collect(Collectors.toList());

		assertTrue(sizeAfter == sizeBefore-1);
		assertFalse(idsAfter.stream().anyMatch(id -> id==idDeleted));
	}

	@Test
	public void testDeleteAll() {
		List<Person> listBefore = new ArrayList<>(peopleRepositoryMock.getPeople());

		// delete all person
		listBefore.forEach(person -> assertTrue(peopleRepositoryMock.deleteById(person.getId()).isPresent()));

		List<Person> listAfter = peopleRepositoryMock.getPeople();

		assertEquals(0, listAfter.size());

		// try to delete first again
		assertFalse(peopleRepositoryMock.deleteById(listBefore.get(0).getId()).isPresent());

	}
}
