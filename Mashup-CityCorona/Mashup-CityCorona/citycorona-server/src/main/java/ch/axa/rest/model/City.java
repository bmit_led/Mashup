package ch.axa.rest.model;

public class City {

	private int count;
	private String path;
	private String state;
	private String date;
	
	public City(int count, String path, String state, String date) {
		super();
		this.count = count;
		this.path = path;
		this.state = state;
		this.date = date;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}
