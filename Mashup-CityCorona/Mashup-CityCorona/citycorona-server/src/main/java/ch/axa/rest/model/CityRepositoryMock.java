package ch.axa.rest.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Repository;

@Repository
public class CityRepositoryMock {

	private List<City> city = null;
	
	public CityRepositoryMock() {
		city = new ArrayList<>();
	}
	public List<City> getCity() {
		return city;
	}
	
	public Optional<City> getCity(String path) {
		return city.stream()
				.filter(c -> c.getPath()==path)
				.findFirst();
	}
}
