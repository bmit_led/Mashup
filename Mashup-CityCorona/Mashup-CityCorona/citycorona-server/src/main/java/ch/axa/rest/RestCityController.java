package ch.axa.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ch.axa.rest.model.City;
import ch.axa.rest.model.CityRepositoryMock;
import ch.axa.rest.model.Person;


@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class RestCityController {

	private CityRepositoryMock cityRepository;

	@Autowired
    public RestCityController(CityRepositoryMock cityRepository                                ) {
        this.cityRepository = cityRepository;
    }

    @GetMapping("/city")
    public ResponseEntity<List<City>> getCity() {
        return ResponseEntity
                .status(HttpStatus.OK) // HTTP 200
                .contentType(MediaType.APPLICATION_JSON)
                .body(cityRepository.getCity());
    }
    
    @PostMapping("/city")
    public ResponseEntity<City> addCity(@RequestBody City city) {
        return null; //TODO
    }
    
    @GetMapping("/saveData")
    public SaveData<List<City>> getCity() {
    	String json = new Gson().toJson(cityRepository.getCity());
    	try {
    		file = new FileWriter("Mashup-CityCorona/Mashup-CityCorona/citycorona-server/src/main/saveddata.json");
    		file.write(json);
    		
    	} catch (IOException e) {
            e.printStackTrace();
 
        } finally {
 
            try {
                file.flush();
                file.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    	return ResponseEntity
    			..status(HttpStatus.OK) // HTTP 200
    }
    
    
    @PatchMapping("/cityedit/{name}")
    public ResponseEntity<?> personEdit(@PathVariable String name) {
    	if(cityRepository.cityEdit(name).isPresent()) {
    		return ResponseEntity.noContent().build();
    	} else {
            return ResponseEntity.notFound().build();   // HTTP 404
        }
    }
    
    
}
