import React, { useEffect, useState } from 'react';

import './App.css';



export default function App() {
  const [counter, setCounter] = useState(0)
  const [message, setMessage] = useState("")
  
  useEffect(() => {
    console.log("useEffect")
    setMessage("My counter is: " + counter)
  }, [counter])

  function handleButton() {
    setCounter(counter+1)
  }

  console.log("in App")
  return (
    <div>
      App
      <br />
      {message}
      <br/>
      <button onClick={handleButton}>count</button>
    </div>
  )
}