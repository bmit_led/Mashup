import React, { useEffect, useState } from 'react';

import './App.css';


export default function SingleCards() {
  const [cardDeckId, setCarddDeckId] = useState('-')
  const [message, setMessage] = useState('')
  const [cards, setCards] = useState([])
  const [cardURL, setCardURL] = useState(undefined)
  
  useEffect(() => {
    fetch('https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1')
    .then(response => response.json())
    .then(cardDeck => setCarddDeckId(cardDeck.deck_id))
  }, [])

  useEffect(() => {
    setMessage("My CardDeck-ID is: " + cardDeckId)
  }, [cardDeckId])

  useEffect(() => {
    if (cards.length>0) {
        setCardURL(cards[cards.length-1].image)
    }
  }, [cards])

  

  function btHandlerNewCard() {
    fetch('https://deckofcardsapi.com/api/deck/' + cardDeckId + '/draw/?count=1')
    .then(response => response.json())
    .then(data => {
        /* ugly code "/"
        const cardsC = cards.slice()
        cardsC.push(data.cards[0])
        setCards(cardsC) */

        setCards([...cards, data.cards[0]])
    })
    {
        const example = {
            "success": true,
            "cards": [
                {
                    "image": "https://deckofcardsapi.com/static/img/KH.png",
                    "value": "KING",
                    "suit": "HEARTS",
                    "code": "KH"
                },
                {
                    "image": "https://deckofcardsapi.com/static/img/8C.png",
                    "value": "8",
                    "suit": "CLUBS",
                    "code": "8C"
                }
            ],
            "deck_id":"3p40paa87x90",
            "remaining": 50
        }
    }
  }

  const imageTag = cardURL ? <img src={cardURL} /> : ''
  const cardList = cards.map(card => 
    <img key={card.code} src={card.image} style={{width: '60px'}} />)
  return (
    <div>
      App
      <br />
      {message}
      <br/>
      <button onClick={btHandlerNewCard}>draw next card</button>
      <br />
      {imageTag}
      <br />
      {cardList}
    </div>
  )
}